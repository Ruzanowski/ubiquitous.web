export const environment = {
  production: true,
  hostIP: 'robust-systems.eu',
  apiGatewayPort: '4500',
  productServicePort: '5000',
  smartStoreAdapterPort: '5100',
  fetchServicePort: '5200',
  subscriptionServicePort: '5300',
  identityServicePort: '5400',
  notificationServicePort: '5500',
  signalrPort: '5500',
  signalrEndpoint: 'signalr',
  toastrEnabled: true
};
