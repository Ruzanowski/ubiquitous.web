export interface Manufacturer {
  id: string;
  name: string;
  description: string;
  pictureId?: number;
}
