export interface User
{

  Id: string;
  Email: string;
  Nickname: string;
  Role: string;
  CreatedAt: Date;
  UpdatedAt: Date;
}
