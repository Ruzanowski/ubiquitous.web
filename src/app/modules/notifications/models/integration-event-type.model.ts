export enum IntegrationEventType
{
  Unknown = 0,
  ProductPublishedIntegrationEvent = 1,
  ProductPropertiesChangedIntegrationEvent = 2,
  ProductAddedIntegrationEvent = 3,
  NewProductFetched = 4,
  UserConnected = 5,
  UserDisconnected = 6,
  AccessTokenRefreshedIntegrationEvent = 7,
  SignedUp = 8
}
