import {UserEventBase} from "./user-event-base.model";

export interface UserDisconnectedEvent extends UserEventBase
{
}
